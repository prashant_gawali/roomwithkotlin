package com.example.roomwithkotlin.data

import androidx.lifecycle.LiveData
import androidx.room.Insert
import androidx.room.Query

interface UserDao {
    @Insert
    suspend fun addUser(user: User)

    @Query("SELECT * FROM user_table ORDER BY id ASC")
    fun readAllData(): LiveData<List<User>>


}